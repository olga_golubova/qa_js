
//Используя switch statement напишите функцию getDaysForMonth, которая принимает 1 параметр: month - означает месяц года 
//(месяц всегда больше 0, меньше или равен 12). И возвращает количество дней в месяце (e.g. 1 вернет 31). 
//Високосный год учитывать не стоит.

function getDaysForMonth(month=1){
	
    var days = 0;
	
	if (isNaN(month)|| month<=0||month>12)  {
		console.log('Wrong dates!');
		return days;
	}
	
    switch (month) {
        case 2: // February. add leap year correction
            days = 28;
            break;
        case 3: case 5: case 8: case 10: 
            days = 30;
            break;
        default: 
            days = 31;
        }
	return days;
}


console.log("February", getDaysForMonth(month=2));
console.log("march", getDaysForMonth(month=3));
console.log("september", getDaysForMonth(month=9));
console.log("zero month", getDaysForMonth(month=0));
console.log("letter month", getDaysForMonth(month='febr'));
