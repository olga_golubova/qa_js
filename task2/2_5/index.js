function getDiscount(number = 1)
{
	var discount = 1;
	
	if (isNaN(number)) {
		console.log('Not a Number!');
		return discount;
	}
	discount = (number < 5) ? 1 :
        ((number >= 5) && (number < 10)) ? 0.95 :0.9; 
	return discount;
}

console.log('скидка (должан быть 0.95) = ',getDiscount(7));
console.log('скидка (должан быть 0.95)= ',getDiscount(5));
console.log('скидка (должан быть 1)= ',getDiscount(4));
console.log('скидка (должан быть 1)= ',getDiscount(0));
console.log('скидка (должан быть 0)= ',getDiscount("fsdfsd"));
console.log('скидка (должан быть 0)= ',getDiscount("-1"));

