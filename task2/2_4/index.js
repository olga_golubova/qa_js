function findTheOne(arr){
       	 	let unique = [...new Set(arr)];
        	return arr.filter(x => x==unique[0]).length>1?unique[1]:unique[0]; 
        }      
		
var arraynumbers1 = [0.56, 0.56, 0.56, 0.56, 1, 0.56];
console.log(findTheOne(arraynumbers1));

var arraynumbers2 = [1, 1, 0.56, 1, 1, 1];
console.log(findTheOne(arraynumbers2));

var arraynumbers3 = [1, 1, 10, 1, 1, 1];
console.log(findTheOne(arraynumbers3));

var arraynumbers4 = [7, 7, 7.7, 7, 7, 7];
console.log(findTheOne(arraynumbers4));

var arraynumbers5 = [27, 27, 27, 27, 27, 27];
console.log(findTheOne(arraynumbers5));