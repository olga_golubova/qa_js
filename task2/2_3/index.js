function compareDate(d1,d2){
			var a1 = new Date(d1.replace(/[.,\/#!$%\^&\*;:{}=\-_`~()]/g,"/"));
			var a2 = new Date(d2.replace(/[.,\/#!$%\^&\*;:{}=\-_`~()]/g,"/"));
        	return (a1.getTime() === a2.getTime());
        } 
		
//test
d1 = "1982.10.10";
d2 = "10/10/1982";
console.log('compareDate1 -> ',compareDate(d1,d2));

d1 = "1982.11.10";
d2 = "10.10/1982";
console.log('compareDate -> ',compareDate(d1,d2));